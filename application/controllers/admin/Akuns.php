<?php

class Akuns extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("akun_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        
        // jika form login disubmit
        if($this->input->post()){
            if($this->akun_model->doLogin()) redirect(site_url('admin'));       
        }

        // tampilkan halaman login
        $this->load->view('admin/login_page');    
    }

    public function logout()
    {
        // hancurkan semua sesi
        $this->session->sess_destroy();
        redirect(site_url('admin/akuns'));
    }
    public function dataakun()
    {
        $data["akuns"] = $this->akun_model->getAll();
        $this->load->view("admin/data_booktix/dataAkun/list_akun", $data);
    }
    public function add()
    {
        $akuns = $this->akun_model;
        $validation = $this->form_validation;
        $validation->set_rules($akuns->rules());

        if ($validation->run()) {
            $akuns->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/data_booktix/dataAkun/add_akun");
    }
    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/akuns');
       
        $akuns = $this->akun_model;
        $validation = $this->form_validation;
        $validation->set_rules($akuns->rules());

        if ($validation->run()) {
            $akuns->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["akuns"] = $akuns->getById($id);
        if (!$data["akuns"]) show_404();
        
        $this->load->view("admin/data_booktix/dataAkun/edit_akun", $data);
    }
    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->akun_model->delete($id)) {
            redirect(site_url('admin/akuns/dataakun'));
        }
    }
    public function cetak()
    {
        $data['akuns'] = $this->akun_model->getAll();
        $this->load->view('admin/data_booktix/dataAkun/cetak_akun', $data);
    }
    public function register()
      {
        $this->load->view("admin/register");
      }
      public function add_register()
    {
        $akuns = $this->akun_model;
        $validation = $this->form_validation;
        $validation->set_rules($akuns->rules());

        if ($validation->run()) {
        $akuns->register_save();
        $this->session->set_flashdata('success', 'Berhasil Silahkan Login');
        }

        $this->load->view("admin/register");
    }
}