<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kursi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("kursi_model");
        $this->load->library('form_validation');
        $this->load->model("akun_model");
        if($this->akun_model->isNotLogin()) redirect(site_url('admin/akuns'));
    }
    public function index()
    {
        $data["kursi"] = $this->kursi_model->getAll();
        $this->load->view("admin/data_booktix/kursi/list_kursi", $data);
    }
    public function add()
    {
        $kursi = $this->kursi_model;
        $validation = $this->form_validation;
        $validation->set_rules($kursi->rules());

        if ($validation->run()) {
            $kursi->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/data_booktix/kursi/add_kursi");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/kursi');
       
        $kursi = $this->kursi_model;
        $validation = $this->form_validation;
        $validation->set_rules($kursi->rules());

        if ($validation->run()) {
            $kursi->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["kursi"] = $kursi->getById($id);
        if (!$data["kursi"]) show_404();
        
        $this->load->view("admin/data_booktix/kursi/edit_kursi", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->kursi_model->delete($id)) {
            redirect(site_url('admin/kursi'));
        }
    }
    public function cetak()
    {
        $data['kursi'] = $this->kursi_model->getAll();
        $this->load->view('admin/data_booktix/kursi/cetak_kursi', $data);
    }
}