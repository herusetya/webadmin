<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Films extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("film_model");
        $this->load->library('form_validation');
        $this->load->model("akun_model");
        if($this->akun_model->isNotLogin()) redirect(site_url('admin/akuns'));
    }
    public function index()
    {
        $data["films"] = $this->film_model->getAll();
        $this->load->view("admin/data_booktix/film/list_film", $data);
    }
    public function add()
    {
        $film = $this->film_model;
        $validation = $this->form_validation;
        $validation->set_rules($film->rules());

        if ($validation->run()) {
            $film->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/data_booktix/film/add_film");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/films');
       
        $film = $this->film_model;
        $validation = $this->form_validation;
        $validation->set_rules($film->rules());

        if ($validation->run()) {
            $film->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["film"] = $film->getById($id);
        if (!$data["film"]) show_404();
        
        $this->load->view("admin/data_booktix/film/edit_film", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->film_model->delete($id)) {
            redirect(site_url('admin/films'));
        }
    }
    public function cetak()
    {
        $data['films'] = $this->film_model->getAll();
        $this->load->view('admin/data_booktix/film/cetak_film', $data);
    }
}