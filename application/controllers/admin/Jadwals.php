<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwals extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("jadwal_model");
        $this->load->library('form_validation');
        $this->load->model("akun_model");
        if($this->akun_model->isNotLogin()) redirect(site_url('admin/akuns'));
    }
    public function index()
    {
        $data["jadwals"] = $this->jadwal_model->getAll();
        $this->load->view("admin/data_booktix/jadwal/list_jadwal", $data);
    }
    public function add()
    {
        $jadwal = $this->jadwal_model;
        $validation = $this->form_validation;
        $validation->set_rules($jadwal->rules());

        if ($validation->run()) {
            $jadwal->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/data_booktix/jadwal/add_jadwal");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/jadwals');
       
        $jadwal = $this->jadwal_model;
        $validation = $this->form_validation;
        $validation->set_rules($jadwal->rules());

        if ($validation->run()) {
            $jadwal->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["jadwal"] = $jadwal->getById($id);
        if (!$data["jadwal"]) show_404();
        
        $this->load->view("admin/data_booktix/jadwal/edit_jadwal", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->jadwal_model->delete($id)) {
            redirect(site_url('admin/jadwals'));
        }
    }
    public function cetak()
    {
        $data['jadwals'] = $this->jadwal_model->getAll();
        $this->load->view('admin/data_booktix/jadwal/cetak_jadwal', $data);
    }
}