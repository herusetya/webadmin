<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayarans extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pembayaran_model");
        $this->load->library('form_validation');
        $this->load->model("akun_model");
        if($this->akun_model->isNotLogin()) redirect(site_url('admin/akuns'));
    }
    public function index()
    {
        $data["pembayarans"] = $this->pembayaran_model->getAll();
        $this->load->view("admin/data_booktix/pembayaran/list_pembayaran", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->pembayaran_model->delete($id)) {
            redirect(site_url('admin/pembayarans'));
        }
    }
    public function cetak()
    {
        $data['pembayarans'] = $this->pembayaran_model->getAll();
        $this->load->view('admin/data_booktix/pembayaran/cetak_pembayaran', $data);
    }
}