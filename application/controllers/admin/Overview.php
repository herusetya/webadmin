<?php

class Overview extends CI_Controller {
    public function __construct()
    {
    parent::__construct();
  }

  public function index()
  {
        // load view admin/overview.php
        $this->load->view("admin/overview");
  }
  public function admbooktix()
  {
        $this->load->view("admin/cobatmp/admbooktix");
  }
  /////////////////////////////////////////////////////////////////////////////////
  public function register()
  {
    $this->load->view("admin/cobatmp/register");
  }
  public function forgot_pw()
  {
    $this->load->view("admin/cobatmp/forgot-password");
  }
  public function empatkosong()
  {
    $this->load->view("admin/cobatmp/404");
  }
  public function blank()
  {
    $this->load->view("admin/cobatmp/blank");
  }
  /////////////////////////////////////////////////////////////////////////////////////
  public function buttons()
  {
    $this->load->view("admin/cobatmp/buttons");
  }
  public function cards()
  {
    $this->load->view("admin/cobatmp/cards");
  }
  public function charts()
  {
    $this->load->view("admin/cobatmp/charts");
  }
  public function tables()
  {
    $this->load->view("admin/cobatmp/tables");
  }
  //////////////////////////////////////////////////////////////////////////////////
  public function ut_color()
  {
    $this->load->view("admin/cobatmp/ut-color");
  }
  public function ut_border()
  {
    $this->load->view("admin/cobatmp/ut-border");
  }
  public function ut_anima()
  {
    $this->load->view("admin/cobatmp/ut-animation");
  }
  public function ut_other()
  {
    $this->load->view("admin/cobatmp/ut-other");
  }
}
?>