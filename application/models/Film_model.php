<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Film_model extends CI_Model
{
    private $_table = "film";

    public $id_film;
    public $judul;
    public $kategori;
    public $durasi;
    public $gambar;
    public $deskripsi;
    public $jadwal;
    public $studio;

    public function rules()
    {
        return [
            ['field' => 'judul',
            'label' => 'Judul',
            'rules' => 'required'],

            ['field' => 'kategori',
            'label' => 'Kategori',
            'rules' => 'required'],
            
            ['field' => 'durasi',
            'label' => 'Durasi',
            'rules' => 'required'],

            ['field' => 'gambar',
            'label' => 'Gambar',
            'rules' => 'required'],

            ['field' => 'deskripsi',
            'label' => 'Deskripsi',
            'rules' => 'required'],

            ['field' => 'jadwal',
            'label' => 'Jadwal',
            'rules' => 'required'],
            
            ['field' => 'studio',
            'label' => 'Studio',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_film" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_film = [""];
        $this->judul = $post["judul"];
        $this->kategori = $post["kategori"];
        $this->durasi = $post["durasi"];
        $this->gambar = $post["gambar"];
        $this->deskripsi = $post["deskripsi"];
        $this->jadwal = $post["jadwal"];
        $this->studio = $post["studio"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_film = $post["id"];
        $this->judul = $post["judul"];
        $this->kategori = $post["kategori"];
        $this->durasi = $post["durasi"];
        $this->gambar = $post["gambar"];
        $this->deskripsi = $post["deskripsi"];
        $this->jadwal = $post["jadwal"];
        $this->studio = $post["studio"];
        return $this->db->update($this->_table, $this, array('id_film' => $post['id']));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_film" => $id));
    }
}