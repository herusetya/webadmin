<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kursi_model extends CI_Model
{
    private $_table = "kursi";

    public $id_kursi;
    public $no_kursi;
    public $status;
    public $studio;

    public function rules()
    {
        return [

            ['field' => 'no_kursi',
            'label' => 'No_kursi',
            'rules' => 'required'],
            
            ['field' => 'status',
            'label' => 'Status',
            'rules' => 'required'],
            
            ['field' => 'studio',
            'label' => 'Studio',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_kursi" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_kursi = [""];
        $this->no_kursi = $post["no_kursi"];
        $this->status = $post["status"];
        $this->studio = $post["studio"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_kursi = $post["id"];
        $this->no_kursi = $post["no_kursi"];
        $this->status = $post["status"];
        $this->studio = $post["studio"];
        return $this->db->update($this->_table, $this, array('id_kursi' => $post['id']));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_kursi" => $id));
    }
}