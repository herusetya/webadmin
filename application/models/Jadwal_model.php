<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends CI_Model
{
    private $_table = "waktu_film";

    public $id_waktu;
    public $waktu;
    public $hari;

    public function rules()
    {
        return [
            
            ['field' => 'waktu',
            'label' => 'Waktu',
            'rules' => 'required'],

            ['field' => 'hari',
            'label' => 'Hari',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_waktu" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_waktu = [""];
        $this->waktu = $post["waktu"];
        $this->hari = $post["hari"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_waktu = $post["id"];
        $this->waktu = $post["waktu"];
        $this->hari = $post["hari"];
        return $this->db->update($this->_table, $this, array('id_waktu' => $post['id']));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_waktu" => $id));
    }
}