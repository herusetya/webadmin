<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends CI_Model
{
    private $_table = "pembayaran";

    public $id;
    public $username;
    public $judulfilm;
    public $total_bayar;
    public $gambar;
    public $no_kursi;
    public $id_user;
    public $id_booking;

    public function rules()
    {
        return [
            ['field' => 'username',
            'label' => 'username',
            'rules' => 'required'],

            ['field' => 'gambar',
            'label' => 'Gambar',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_film = [""];
        $this->judul = $post["judul"];
        $this->durasi = $post["durasi"];
        $this->gambar = $post["gambar"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_film = $post["id"];
        $this->judul = $post["judul"];
        $this->durasi = $post["durasi"];
        $this->gambar = $post["gambar"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }
}