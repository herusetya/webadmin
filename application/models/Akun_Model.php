<?php

class Akun_model extends CI_Model
{
    private $_table = "akun";

    public function doLogin(){
		$post = $this->input->post();

        // cari user berdasarkan email dan username
        $this->db->where('email', $post["email"])
                ->or_where('username', $post["email"]);
        $user = $this->db->get($this->_table)->row();

        // jika user terdaftar
        if($user){
            //data email
            $data = array(
                'email' => $user->email,
                'foto' => $user->foto,
                );         
            // periksa password-nya
            // $isPasswordTrue = password_verify($post["password"], $user->password)
            $isPasswordTrue = $user->password == $post["password"];
            // periksa role-nya
            $isAdmin = $user->role == "admin";
            // jika password benar dan dia admin
            if($isPasswordTrue && $isAdmin){ 
                // login sukses yay!
                $this->session->set_userdata($data);
                $this->session->set_userdata(['user_logged' => $user]);
                return true;
            }
        }
        
        // login gagal
		return false;
    }

    public function isNotLogin(){
        return $this->session->userdata('user_logged') === null;
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    ////////////////////////////////////////////////////////////////////
    public $id_user;
    public $username;
    public $password;
    public $email;
    public $nama;
    public $nomorhp;
    public $role;
    public $foto = "default.jpg";
    public function rules()
    {
        return [
            ['field' => 'username',
            'label' => 'Username',
            'rules' => 'required'],

            ['field' => 'password',
            'label' => 'Password',
            'rules' => 'required'],

            ['field' => 'email',
            'label' => 'Email',
            'rules' => 'required']
        ];
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_user" => $id])->row();
    }
    public function save()
    {
        $post = $this->input->post();
        $this->id_user = uniqid();
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->email = $post["email"];
        $this->nama = $post["nama"];
        $this->nomorhp = $post["nomorhp"];
        $this->role = $post["role"];
        $this->foto = $this->_uploadImage();
        return $this->db->insert($this->_table, $this);
    }
    public function update()
    {
        $post = $this->input->post();
        $this->id_user = $post["id"];
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->email = $post["email"];
        $this->nama = $post["nama"];
        $this->nomorhp = $post["nomorhp"];
        $this->role = $post["role"];
        if (!empty($_FILES["foto"]["name"])) {
            $this->foto = $this->_uploadImage();
        } else {
            $this->foto = $post["old_image"];
        }
        return $this->db->update($this->_table, $this, array('id_user' => $post['id']));
    }
    public function delete($id)
    { 
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id_user" => $id));
    }
    private function _uploadImage()
    {
        $config['upload_path']          = './upload/foto_akun/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id_user;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }
        print_r($this->upload->display_errors());
        //return "default.jpg";
    }
    private function _deleteImage($id)
    {
        $akun = $this->getById($id);
        if ($akun->foto != "default.jpg") {
            $filename = explode(".", $akun->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/foto_akun/$filename.*"));
        }
    }
    ////////////////////////////////////////////////////////////////////
    public function register_save()
    {
        $post = $this->input->post();
        $this->id_user = uniqid();
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->email = $post["email"];
        $this->nama = "Edit_Nama";
        $this->nomorhp = "Edit_Nomorhp";
        $this->role = "admin";
        return $this->db->insert($this->_table, $this);
    }
}