<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo SITE_NAME .": ". ucfirst($this->uri->segment(1)) ." - ". ucfirst($this->uri->segment(2)) ?> - Register</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url ('assets/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url ('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i')?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url ('css/sb-admin-2.min.css')?>" rel="stylesheet">

</head>

<body class="bg-gradient-primary">


  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="card col-xl-4 col-lg-6 col-md-9 shadow-lg my-5">

          <div class="shadow-lg my-5">

          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
                <div class="p-5">
                  <div class="text-center">
                  <?php if ($this->session->flashdata('success')): ?>
                  <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <?php endif; ?>
                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                  </div>
                  <form action="<?= site_url('admin/akuns/add_register') ?>" method="POST">

                    <div class="form-group">
                      <input class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?> type="text" name="username" class="form-control form-control-user" id="username" aria-describedby="emailHelp" placeholder="Username" />
                      <div class="invalid-feedback">
                       <?php echo form_error('username') ?>
                       </div>
                    </div>

                    <div class="form-group">
                      <input class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?> type="password" name="password" class="form-control form-control-user" id="password" placeholder="Password">
                      <div class="invalid-feedback">
                       <?php echo form_error('password') ?>
                       </div>
                    </div>

                    <div class="form-group">
                      <input class="form-control <?php echo form_error('email') ? 'is-invalid':'' ?> type="text" name="email" class="form-control form-control-user" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                      <div class="invalid-feedback">
                       <?php echo form_error('email') ?>
                       </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-user btn-block" value="Register" />
                    </div>
                    <hr>
                  </form>
              <div class="text-center">
                <a class="small" href="<?php echo site_url('Admin/Akuns/index') ?>">Login here!</a>
              </div>

                </div>
              </div>
            </div>
          </div>

          </div>

        </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url ('aseets/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url ('vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url ('aseets/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url ('js/sb-admin-2.min.js')?>"></script>

</body>

</html>
