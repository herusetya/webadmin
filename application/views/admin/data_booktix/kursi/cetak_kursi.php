<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("admin/_partials/1_head") ?>
</head>
<body>
<h1><center>LAPORAN DATA KURSI</center></h1>
<table class="table table-bordered table-head-bg-default table-bordered-bd-info table-striped table-hover mt-4">
	<tr>
		<th scope="col">#</th>
    <th scope="col">No Kursi</th>
    <th scope="col">Status</th>
    <th scope="col">Studio</th>
	</tr>
	<?php $no=1; foreach ($kursi as $kursip): ?>
    <tr>
      <td>
        <?php echo $no++ ?>
      </td>
      <td>
        <?php echo $kursip->no_kursi ?>
      </td>
      <td>
        <?php echo $kursip->status ?>
      </td>
      <td>
        <?php echo $kursip->studio ?>
      </td>
      </tr>
  <?php endforeach; ?>
</table>
<!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>
	<script type="text/javascript">
		window.print();
	</script>

</body>
</html>