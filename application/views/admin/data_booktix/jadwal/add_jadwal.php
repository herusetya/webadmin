<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view("admin/_partials/1_head") ?>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?php $this->load->view("admin/_partials/2_sidebar") ?>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <?php $this->load->view("admin/_partials/3_topbar") ?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php endif; ?>

        <div class="container-fluid bg-gradient-white">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Data Jadwal</h1>
          </div>
        </div>

        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/jadwals/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
          </div>
        </div>

          <div class="container">
          <div class="row">
            
          <div class="card shadow mb-4 col-lg-2">
          </div>  
          <div class="card shadow mb-4 col-lg-4">

            <form action="<?php echo site_url('admin/jadwals/add') ?>" method="post" enctype="multipart/form-data" >
              <div class="form-group">
                <input class="form-control <?php echo form_error('waktu') ? 'is-invalid':'' ?>"
                 type="text" name="waktu" placeholder="Input Waktu" />
                <div class="invalid-feedback">
                  <?php echo form_error('waktu') ?>
                </div>
              </div>
              <div class="form-group">
                <input class="form-control <?php echo form_error('hari') ? 'is-invalid':'' ?>"
                 type="text" name="hari" placeholder="Input Hari" />
                <div class="invalid-feedback">
                  <?php echo form_error('hari') ?>
                </div>
              </div>

              <input class="btn btn-success" type="submit" name="btn" value="Simpan" />
            </form>

          </div>
          <div class="card shadow mb-4 col-lg-2">
          </div>  

          </div>
          </div>

          <div class="card-footer small text-muted">
            * required fields
          </div>


        
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php $this->load->view("admin/_partials/5_footer") ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!--Modal-->
  <?php $this->load->view("admin/_partials/6_modal") ?>
  <!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>

</body>

</html>
