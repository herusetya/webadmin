<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("admin/_partials/1_head") ?>
</head>
<body>
<h1><center>LAPORAN DATA JADWAL FILM</center></h1>
<table class="table table-bordered table-head-bg-default table-bordered-bd-info table-striped table-hover mt-4">
	<tr>
		<th scope="col">#</th>
    <th scope="col">Waktu</th>
    <th scope="col">Hari</th>
	</tr>
	 <?php 
    $no= 1;
    foreach ($jadwals as $jadwal): ?>

    <tr>
      <td><?php echo $no++ ?></td>
      <td>
        <?php echo $jadwal->waktu ?>
      </td>
      <td>
        <?php echo $jadwal->hari ?>
      </td>
      </tr>
  <?php endforeach; ?>
</table>
<!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>
	<script type="text/javascript">
		window.print();
	</script>

</body>
</html>