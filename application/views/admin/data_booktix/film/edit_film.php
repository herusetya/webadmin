<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view("admin/_partials/1_head") ?>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?php $this->load->view("admin/_partials/2_sidebar") ?>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <?php $this->load->view("admin/_partials/3_topbar") ?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php endif; ?>

        <div class="container-fluid bg-gradient-white">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Data Film</h1>
          </div>
        </div>

        <!-- Card  -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/films/') ?>"><i class="fas fa-arrow-left"></i>
              Back</a>
          </div>
        </div>

        <div class="container">
          <div class="row">

            <div class="card shadow mb-4 col-lg-2">
            </div> 
            <div class="card shadow mb-4 col-lg-4">
            <form action="" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id" value="<?php echo $film->id_film?>" />

                    <div class="form-group">         
                      <input aria-label="" class="form-control <?php echo form_error('judul') ? 'is-invalid':'' ?>"
                       type="text" name="judul" placeholder="Ubah Judul" value="<?php echo $film->judul ?>" />
                      <div class="invalid-feedback">
                        <?php echo form_error('judul') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <input class="form-control <?php echo form_error('kategori') ? 'is-invalid':'' ?>"
                       type="text" name="kategori" placeholder="Ubah Kategori" value="<?php echo $film->kategori ?>"/>
                      <div class="invalid-feedback">
                        <?php echo form_error('kategori') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <input class="form-control <?php echo form_error('durasi') ? 'is-invalid':'' ?>"
                       type="text" name="durasi" placeholder="Ubah Durasi" value="<?php echo $film->durasi ?>"/>
                      <div class="invalid-feedback">
                        <?php echo form_error('durasi') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <input class="form-control <?php echo form_error('gambar') ? 'is-invalid':'' ?>"
                       type="text" name="gambar" placeholder="Ubah Url Gambar" value="<?php echo $film->gambar ?>"/>
                      <div class="invalid-feedback">
                        <?php echo form_error('gambar') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <textarea class="form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>"
                       name="deskripsi" placeholder="Ubah Deskripsi..."><?php echo $film->deskripsi ?></textarea>
                      <div class="invalid-feedback">
                        <?php echo form_error('deskripsi') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <input class="form-control <?php echo form_error('jadwal') ? 'is-invalid':'' ?>"
                       type="text" name="jadwal" placeholder="Ubah Jadwal" value="<?php echo $film->jadwal ?>"/>
                      <div class="invalid-feedback">
                        <?php echo form_error('jadwal') ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <input class="form-control <?php echo form_error('studio') ? 'is-invalid':'' ?>"
                       type="text" name="studio" placeholder="Ubah Studio" value="<?php echo $film->studio ?>"/>
                      <div class="invalid-feedback">
                        <?php echo form_error('studio') ?>
                      </div>
                    </div>

                    <input class="btn btn-success" type="submit" name="btn" value="Simpan" />
            </form>
            </div>
            <div class="card shadow mb-4 col-lg-2">
            </div>  

          </div>
        </div>

          <div class="card-footer small text-muted">
            * required fields
          </div>


        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php $this->load->view("admin/_partials/5_footer") ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!--Modal-->
  <?php $this->load->view("admin/_partials/6_modal") ?>
  <!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>

</body>

</html>
