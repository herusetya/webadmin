<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("admin/_partials/1_head") ?>
</head>
<body>
<h1><center>LAPORAN DATA FILM</center></h1>
<table class="table table-bordered table-head-bg-default table-bordered-bd-info table-striped table-hover mt-4">
	<tr>
		<th>#</th>
        <th>Judul</th>
        <th>Kategori</th>
        <th>Durasi</th>
        <th >Gambar</th>
        <th>Deskripsi</th>
        <th>Jadwal</th>
        <th>Studio</th>
	</tr>
	<?php foreach ($films as $film): ?>
      <tr>
        <td>
          <?php echo $film->id_film ?>
        </td>
        <td>
          <?php echo $film->judul ?>
        </td>
        <td>
          <?php echo $film->kategori ?>
        </td>
        <td>
          <?php echo $film->durasi ?>
        </td>
        <td class="small">
          <a href="<?php echo $film->gambar ?>">
          <img src="<?php echo $film->gambar ?>" width="64" height="67"/>
          <?php echo substr($film->gambar, 0, 13) ?>...</a></td>
        <td class="small">
          <?php echo substr($film->deskripsi, 0, 30) ?>...</td>
        <td>
          <?php echo $film->jadwal ?>
        </td>
        <td>
          <?php echo $film->studio ?>
        </td>
      </tr>
      <?php endforeach; ?>
</table>
<!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>
	<script type="text/javascript">
		window.print();
	</script>

</body>
</html>