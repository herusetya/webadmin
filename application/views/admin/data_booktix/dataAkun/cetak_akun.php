<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("admin/_partials/1_head") ?>
</head>
<body>
<h1><center>LAPORAN DATA AKUN</center></h1>
<table class="table table-bordered table-head-bg-default table-bordered-bd-info table-striped table-hover mt-4">
	<tr>
		<th scope="col">#</th>
    <th scope="col">Username</th>
    <th scope="col">Password</th>
    <th scope="col">Email</th>
    <th scope="col">Nama</th>
    <th scope="col">NomorHp/WA</th>
    <th scope="col">Role</th>
    <th scope="col">Foto</th>
	</tr>
	 <?php $no=1;foreach ($akuns as $datalogin): ?>
  <tr>
    <td>
      <?php echo $no++ ?>
    </td>
    <td>
      <?php echo $datalogin->username ?>
    </td>
    <td class="small">
      <?php echo substr ($datalogin->password,0,10)?>..
    </td>
    <td>
      <?php echo $datalogin->email ?>
    </td>
    <td>
      <?php echo $datalogin->nama ?>
    </td>
    <td>
      <?php echo $datalogin->nomorhp ?>
    </td>
    <td>
      <?php echo $datalogin->role ?>
    </td>
    <td>
      <img src="<?php echo base_url('upload/foto_akun/'.$datalogin->foto) ?>" width="64" height="64"/>
    </td>
  </tr>
  <?php endforeach; ?>
</table>
<!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>
	<script type="text/javascript">
		window.print();
	</script>

</body>
</html>