<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view("admin/_partials/1_head") ?>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?php $this->load->view("admin/_partials/2_sidebar") ?>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <?php $this->load->view("admin/_partials/3_topbar") ?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid bg-gradient-white">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data Akun</h1>
            <a href="<?php echo site_url('admin/akuns/cetak') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-print fa-sm text-white-50"></i> Cetak Laporan</a>
          </div>
        </div>
         <!-- DataTables -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/akuns/add') ?>"><i class="fas fa-plus"></i> Tambah Data Akun</a>
          </div>
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-head-bg-default table-striped table-hover mt-4" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID Users</th>
                    <th scope="col">Username</th>
                    <th scope="col">Password</th>
                    <th scope="col">Email</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NomorHp/WA</th>
                    <th scope="col">Role</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1;foreach ($akuns as $datalogin): ?>
                  <tr>
                    <td>
                      <?php echo $no++ ?>
                    </td>
                    <td>
                      <?php echo $datalogin->id_user ?>
                    </td>
                    <td>
                      <?php echo $datalogin->username ?>
                    </td>
                    <td class="small">
                      <?php echo substr ($datalogin->password,0,10)?>..
                    </td>
                    <td>
                      <?php echo $datalogin->email ?>
                    </td>
                    <td>
                      <?php echo $datalogin->nama ?>
                    </td>
                    <td>
                      <?php echo $datalogin->nomorhp ?>
                    </td>
                    <td>
                      <?php echo $datalogin->role ?>
                    </td>
                    <td>
                      <img src="<?php echo base_url('upload/foto_akun/'.$datalogin->foto) ?>" width="64" height="64"/>
                    </td>
                    <td width="250">
                      <a href="<?php echo site_url('admin/akuns/edit/'.$datalogin->id_user) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/akuns/delete/'.$datalogin->id_user) ?>')"
                       href="#!" data-target="#deleteModal" data-toggle="modal" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php $this->load->view("admin/_partials/5_footer") ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!-- Modal-->
  <?php $this->load->view("admin/_partials/6_modal") ?>
  <!-- Logout Delete Confirmation-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a id="btn-delete" class="btn btn-danger" href="<?php echo site_url('admin/akuns/delete/'.$datalogin->id_user) ?>">Delete</a>
          </div>
        </div>
      </div>
    </div>
  <!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>

  <script>
  function deleteConfirm(url){
    $('#btn-delete').attr('href', url);
    $('#deleteModal').modal();
  } 
  </script>

</body>

</html>