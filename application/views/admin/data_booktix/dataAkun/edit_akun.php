<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view("admin/_partials/1_head") ?>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?php $this->load->view("admin/_partials/2_sidebar") ?>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <?php $this->load->view("admin/_partials/3_topbar") ?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php endif; ?>

        <div class="container-fluid bg-gradient-white">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Data Akun</h1>
          </div>
        </div>

        <!-- Card  -->
        <div class="card mb-3">
          <div class="card-header">
            <a href="<?php echo site_url('admin/akuns/dataakun') ?>"><i class="fas fa-arrow-left"></i>
              Back</a>
          </div>
        </div>

        <div class="container">
          <div class="row">

            <div class="card shadow mb-4 col-lg-2">
            </div> 
            <div class="card shadow mb-4 col-lg-4">
            <form action="" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id" value="<?php echo $akuns->id_user?>" />

                    <div class="form-group">
                    <input class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>"
                     type="text" name="username" placeholder="Input Username" value="<?php echo $akuns->username?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('username') ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <input class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>"
                     type="text" name="password" placeholder="Input Passwaord" value="<?php echo $akuns->password?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('password') ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <input class="form-control <?php echo form_error('email') ? 'is-invalid':'' ?>"
                     type="text" name="email" placeholder="Input Email" value="<?php echo $akuns->email?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('email') ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>"
                     type="text" name="nama" placeholder="Input Nama" value="<?php echo $akuns->nama?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('nama') ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <input class="form-control <?php echo form_error('nomorhp') ? 'is-invalid':'' ?>"
                     type="text" name="nomorhp" placeholder="Input NomorHp/WA" value="<?php echo $akuns->nomorhp?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('nomorhp') ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <input class="form-control <?php echo form_error('role') ? 'is-invalid':'' ?>"
                     type="text" name="role" placeholder="Input Role" value="<?php echo $akuns->role?>"/>
                    <div class="invalid-feedback">
                      <?php echo form_error('role') ?>
                    </div>
                  </div>
                  <div class="form-group">
                  <span>Input Foto</span>
                    <input class="form-control col-sm-8 mb-5 mb-sm-0 <?php echo form_error('foto') ? 'is-invalid':'' ?>"
                     type="file" name="foto" value="<?php echo $akuns->foto?>"/>
                     <input type="hidden" name="old_image" value="<?php echo $akuns->foto ?>" />
                    <div class="invalid-feedback">
                      <?php echo form_error('foto') ?>
                    </div>
                  </div>
                    

                    <input class="btn btn-success" type="submit" name="btn" value="Simpan" />
            </form>
            </div>
            <div class="card shadow mb-4 col-lg-2">
            </div>  

          </div>
        </div>

          <div class="card-footer small text-muted">
            * required fields
          </div>


        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php $this->load->view("admin/_partials/5_footer") ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!--Modal-->
  <?php $this->load->view("admin/_partials/6_modal") ?>
  <!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>

</body>

</html>
