<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view("admin/_partials/1_head") ?>
</head>
<body>
<h1><center>LAPORAN DATA PEMBAYARAN</center></h1>
<table class="table table-bordered table-head-bg-default table-bordered-bd-info table-striped table-hover mt-4">
	<tr>
		<th scope="col">#</th>
    <th scope="col">UserName</th>
    <th scope="col">Judul Film</th>
    <th scope="col">Total Bayar</th>
    <th scope="col">Bukti Bayar</th>
    <th scope="col">No Kursi</th>
    <th scope="col">ID User</th>
    <th scope="col">ID Booking</th>
	</tr>
	<?php 
  $no= 1;
  foreach ($pembayarans as $pembayaran): ?>

  <tr>
    <td><?php echo $no++ ?></td>
    <td>
      <?php echo $pembayaran->username ?>
    </td>
    <td>
      <?php echo $pembayaran->judulfilm ?>
    </td>
    <td>
      <?php echo $pembayaran->total_bayar ?>
    </td>
    <td class="small">
      <a href="<?php echo $pembayaran->gambar ?>">
      <img src="<?php echo $pembayaran->gambar ?>" width="64" height="64"/>
      </a>
    </td>
    <td>
      <?php echo $pembayaran->no_kursi ?>
    </td>
    <td>
    <?php echo $pembayaran->id_user ?>
    </td>
    <td>
      <?php echo $pembayaran->id_booking ?>
    </td>
  </tr>
  <?php endforeach; ?>
</table>
<!--JavaScript-->
  <?php $this->load->view("admin/_partials/7_js") ?>
	<script type="text/javascript">
		window.print();
	</script>

</body>
</html>