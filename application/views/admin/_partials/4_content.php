<div class="container-fluid bg-gradient-white">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Menu Utama</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>Donwload</a>
          </div>

          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-3 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->       
                <!-- Card Body -->
              </div>             
            </div>
          </div>

          <div class="card shadow mb-4 align-items-center" >
                <h1 class="h3 mb-0 text-gray-800">Mengelola Data Dengan Bijak yaa :D</h1>
                  <!-- Card Header - Dropdown -->       
                  <!-- Card Body -->
          </div>

          <!-- Content Row -->
          <div class="row bg-gradient-success" >
              <div class="col-xl-3 col-lg-7">
              <div class="card shadow col-lg-6 mb-4">
              </div>
            </div>
            <!-- Content Column -->
            <div class="col-lg-6 mb-4">
              <!-- Color System -->
              <div class="row">

                <div class="col-lg-6 mb-4">
                  <div class="card bg-primary text-white shadow">
                    <div class="card-body">
                      Primary
                      <div class="text-white-50 small">#4e73df</div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6 mb-4">
                  <div class="card bg-success text-white shadow">
                    <div class="card-body">
                      Success
                      <div class="text-white-50 small">#1cc88a</div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 mb-4">
                  <div class="card bg-info text-white shadow">
                    <div class="card-body">
                      Info
                      <div class="text-white-50 small">#36b9cc</div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 mb-4">
                  <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                      Warning
                      <div class="text-white-50 small">#f6c23e</div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 mb-4">
                  <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                      Danger
                      <div class="text-white-50 small">#e74a3b</div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 mb-4">
                  <div class="card bg-secondary text-white shadow">
                    <div class="card-body">
                      Secondary
                      <div class="text-white-50 small">#858796</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

</div>