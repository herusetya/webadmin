<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('admin/overview/admbooktix') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo SITE_NAME ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('admin') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Menu Utama</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Data Booktix27
      </div>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsetab" aria-expanded="true" aria-controls="collapsetab">
          <i class="fas fa-fw fa-table"></i>
          <span>Tabel</span>
        </a>
        <div id="collapsetab" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data BooxTix:</h6>
            <a class="collapse-item" href="<?php echo site_url('admin/akuns/dataakun')?>">Akun</a>
            <a class="collapse-item" href="<?php echo site_url('admin/films') ?>">Film</a>
            <a class="collapse-item" href="<?php echo site_url('admin/Jadwals') ?>">Jadwal</a>
            <a class="collapse-item" href="<?php echo site_url('admin/Kursi') ?>">Kursi</a>
            <a class="collapse-item" href="<?php echo site_url('admin/Pembayarans') ?>">Pembayaran</a>
            <div class="collapse-divider"></div>
          </div>
        </div>
      </li>
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Components</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Components:</h6>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/buttons') ?>">Buttons</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/cards') ?>">Cards</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/charts') ?>">Charts</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/tables') ?>">Tables</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class ="collapse-item" href="<?php echo site_url('admin/overview/ut_color') ?>">Colors</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/ut_border') ?>">Borders</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/ut_anima') ?>">Animations</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/ut_other') ?>">Other</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/forgot_pw') ?>">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/empatkosong') ?>">404 Page</a>
            <a class="collapse-item" href="<?php echo site_url('admin/overview/blank') ?>">Blank Page</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Tables Booktix -->
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="" id="sidebarToggle"></button>
      </div>

    </ul>