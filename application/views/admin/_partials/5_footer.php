<footer class="sticky-footer bg-primary">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span class="mr-2 d-none d-lg-inline text-gray-900 big bg-white">Copyright &copy; <?php echo SITE_NAME ?> 2020</span>
          </div>
        </div>
</footer>